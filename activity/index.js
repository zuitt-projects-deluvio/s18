function Pokemon(name, lvl, hp){

		// Properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;
		this.fight = true;

		// methods
		this.tackle = function(target){
	
			if(target.fight === true) {
				if(this.health >= 10) {
					if(target.health >= 10) {

						const initialHealth = target.health;

						target.health = target.health - this.attack;

						console.log(`${this.name} tackled ${target.name}`);
						console.log(`${target.name}'s health is now reduced from ${initialHealth} to ${target.health}`);

					} 

					let checkTargetHealth = target.health;

					if(checkTargetHealth < 10){
							target.faint();
					}	
				} else {
					console.log(`${this.name} can't fight anymore.`)
					this.fight = false;
				}	
			} else if (target.fight === false) {
				console.log(`${this.name} tries to attack ${target.name} but ${target.name} is too weak to fight. Battle end.`)
			}	
				
		};

		this.faint = function(){
			console.log(`${this.name} fainted`)
		}
	}

let pikachu = new Pokemon("Pikachu", 10, 20);
let charizard = new Pokemon("Charizard", 5, 10);


pikachu.tackle(charizard);
charizard.tackle(pikachu);
pikachu.tackle(charizard);

// If charizard still tries to attack even if he already fainted
charizard.tackle(pikachu);

// If pikachu tries to attack charizard who can't fight anymore. Battle end
pikachu.tackle(charizard);